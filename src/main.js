import { createApp } from 'vue';
import App from './App.vue';
import '@/assets/css/tailwind.css';
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import { PulseLoader } from 'vue-spinner/dist/vue-spinner.min.js'

import router from '@/router';

const app = createApp(App)
app.use(Chartkick.use(Chart))
app.use(PulseLoader)
app.use(router)
app.mount('#app');


