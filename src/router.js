import {createRouter,createWebHistory } from 'vue-router';
import Home from '@/views/Home';
import About from '@/views/About';
import Error from '@/views/Error';
import CoinDetail from '@/views/CoinDetail';

export default createRouter({
	history: createWebHistory(),
	routes: 
	[
		{
			path: '/',
			name: 'home',
			component: Home
		},

		{
			path: '/about',
			name: 'about',
			component: About
		},

		{
			path: '/error',
			name: 'error',
			component: Error
		},

		{
			path: '/coin/:id',
			name: 'coin-detail',
			component: CoinDetail
		},

		{
			path: '/:catchAll(.*)',
			name: 'Error',
			component: Error
		}
	]


})